package com.shyndard.service;

import javax.ws.rs.GET;
import javax.ws.rs.Path;

import com.shyndard.entity.Information;

import org.eclipse.microprofile.rest.client.inject.RegisterRestClient;

@RegisterRestClient
public interface InformationService {

    @GET
    @Path("/api/v1/scaleway-evolution")
    Information getInformation();
}