package com.shyndard.entity;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class Information {

    private String title;
    private String message;
    private String status;
    private int love;

    public Information() {

    }

    public Information(String title, String message, String status, int love) {
        this.title = title;
        this.message = message;
        this.status = status;
        this.love = love;
    }

    public String getTitle() {
        return title;
    }

    public String getMessage() {
        return message;
    }

    public String getStatus() {
        return status;
    }

    public int getLove() {
        return love;
    }

    public Information setTitle(String title) {
        this.title = title;
        return this;
    }

    public Information setMessage(String message) {
        this.message = message;
        return this;
    }

    public Information setStatus(String status) {
        this.status = status;
        return this;
    }

    public Information setLove(int love) {
        this.love = love;
        return this;
    }
}
